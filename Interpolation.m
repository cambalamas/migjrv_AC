
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    %%%     Daniel Camba Lamas          %%%
                    %%%        @cambalamas              %%%
                    %%% https://github.com/cambalamas   %%%
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -----------------------------------------------------------------------------

function Interpolation()
    catmull_rom = [2 1 0 3 5]; % Y_values, X will be 0,1,2,...
    catmull(catmull_rom);
    % hermite(-5,0,-5,-5);
    % glagrange([-9 -4 -1 7],[5 2 -2 9]);
    % splines()
end

% -----------------------------------------------------------------------------

function catmull(Y)
figure;
hold on;
% axis equal;


    % a = [2 1 0 3];
    % b = [1 0 3 5];

    h_p1 = (0 - 2) / (2 - 0);
    h_p2 = (3 - 1) / (3 - 1);
    hermite(1,2,1,0,h_p1,h_p2);

    h_p1 = (3 - 1) / (3 - 1);
    h_p2 = (5 - 0) / (4 - 2);
    hermite(2,3,0,3,h_p1,h_p2);

    % x1 = 0;
    % x2 = 1;
    % for i = 1:1:length(Y)-3
    %     px0 = i;    py0 = Y(px0);
    %     px1 = i+1;  py1 = Y(px1);
    %     px2 = i+2;  py2 = Y(px2);
    %     px3 = i+3;  py3 = Y(px3);
    %     h_p1 = (py2 - py0) / (px2 - px0);
    %     h_p2 = (py3 - py1) / (px3 - px1);
    %     hermite(x1,x2,py1,py2,h_p1,h_p2);
    %     x1=x1+1;
    %     x2=x2+1;
    % end

hold off;
end

% -----------------------------------------------------------------------------

function hermite(x0,x1,h0,h1,h2,h3)

    [x0 x1 h0 h1 h2 h3]
    %h
    h = [h0, h1, h2, h3];
    %Polinomios base H (constante para problemas cubicos)
    H = [ 2 -3 0 1;
          -2 3 0 0;
          1 -2 1 0;
          1 -1 0 0 ];
    %parametros por polinomio base
    sol = h*H;

    x = x0:0.0001:x1;
    y = sol(1) * x.^3 + sol(2) * x.^2 + sol(3) * x + sol(4) * 1;
    plot(x,y);

end

% -----------------------------------------------------------------------------

function L = glagrange(x,y)
figure;
hold on;
grid on;
% axis equal;

    y_ = 0;
    x_ = x(1):0.001:x(length(x));
    n = length(x);
    L = ones(n, length(x_));

    for i=1:n
        for j=1:n
            if i~=j, L(i,:) = L(i,:).*(x_-x(j)) / (x(i)-x(j)); end
        end
        Li = y(i) * L(i,:);
        y_ = y_ + Li;
        % plot(x_,Li,'LineWidth',2); % partial plot
    end
        plot(x_,y_,'--','LineWidth',3,'Color', 'red'); % result plot

hold off
end

% -----------------------------------------------------------------------------

function splines()
figure;
hold on;
grid on;

    A = [ 9 3 1 0 0 0  0 0 0;
        20.25 4.5 1 0 0 0 0 0 0;
        0 0 0 20.25 4.5 1 0 0 0;
        0 0 0 49 7 1 0 0 0;
        0 0 0 0 0 0 49 7 1;
        0 0 0 0 0 0 81 9 1;
        9 1 0 -9 -1 0 0 0 0;
        0 0 0 14 1 0 -14 -1 0 ];

    B = [2.5 1 1 2.5 2.5 0.5 0 0]';

    X = A\B;

    % plot 1st segment.
    x = 3:0.01:4.5;
    y = X(1)*x.^2 + X(2)*x + X(3);
    plot(x,y);

    % plot 2nd segment.
    x = 4.5:0.01:7;
    y = X(4)*x.^2 + X(5)*x + X(6);
    plot(x,y);

    % plot 3rd segment.
    x = 7:0.01:9;
    y = X(7)*x.^2 + X(8)*x + X(9);
    plot(x,y);

hold off;
end

% -----------------------------------------------------------------------------
