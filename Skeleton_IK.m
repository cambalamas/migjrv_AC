
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% AC 2017 - Daniel Camba Lamas %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global negative
negative = 1;
main();

function main()
    clc
    clear
    close all

    rig = [  0   [0,0];
             0   [0,-1];
             0   [0,-1];
             0   [0,-1];
             0   [0,-1]  ];

    IK(rig);
end

% 1. LOOP
function IK(rig)
    esc = false;
    lastStatus = 1;

    while ~esc
        err = 1;
        alpha = 0.05;
        g = randGoal();
        iterations = 0;
        [~,e] = DK(rig);

        while err > 0.4 && iterations < 75
            % canvas
            fig(-5,5,' - :: Inverse Kinematic ::  - ', err,iterations,lastStatus);
            % draw the target
            scatter(g(1),g(2),50,'p','black','filled');
            % calc jacobian matrix
            J = calcJacobian(rig,e);
            % calc jacobian pseudoinverse
            iJ = pinv(J);
            % calc angles increment
            newAngles = iJ * (g-e) ;
            % update the angles
            rig(:,1) = rig(:,1) + alpha*newAngles;
            % calc again and plot the rig with new angles
            [sk,e] = DK(rig);
            plotSkeleton(sk);
            % calc again the error
            err = distance(e(1),e(2),g(1),g(2));
            % forces plot
            drawnow;
            % count number of tries
            iterations = iterations + 1;
        end

        if iterations >= 75
            lastStatus = 0;
        else
            lastStatus = 1;
        end

    end
    close all;
end

function J = calcJacobian(rig,ei)
    J = zeros(2,length(rig));
    inc = 0.1;
    for i = 2:1:length(rig)
        % reset auxiliar skeleton
        auxRig = rig;
        % skeleton state when apply increment on bone 'i'
        auxRig(i,1) = auxRig(i,1) + inc;
        % e' = final pos after variation
        [~,em] = DK(auxRig);
        % /\e = em - ei
        e = em - ei;
        % store values on J
        J(1,i) = e(1)/inc;
        J(2,i) = e(2)/inc;
    end
end


% 2. CALC
function [y,p] = DK(rig)
    y = zeros(length(rig),4);
    % simplify data acces with anonymous functions
    trans = @(rig,n)( [ 1 0 rig(n,2); 0 1 rig(n,3); 0 0 1 ] );
    rots  = @(rig,n)( [ +cosd(rig(n,1)) -sind(rig(n,1)) 0; +sind(rig(n,1)) +cosd(rig(n,1)) 0; 0 0 1 ] );
    % first node
    p = rots(rig,1) * trans(rig,1);
    % other nodes
    for i = 1:1:length(rig)
        oldP = p;
        p = oldP * rots(rig,i) * trans(rig,i);
        y(i,:) = [p(1,3) p(2,3) oldP(1,3) oldP(2,3)];
    end
    % p is a matrix 3x3 return only the position itself
    p = [ p(1,3); p(2,3) ];
end

% 3. PLOT
function plotSkeleton(sk)
    for i = 1:1:length(sk)
        line( [sk(i,1),sk(i,3)], [sk(i,2),sk(i,4)], 'LineWidth', 2);
        scatter(sk(i,1), sk(i,2), 300, 'o', 'filled');
    end
end

% --- HELPERS ---

function fig(x,y,str,err,iterations,lastStatus)
    cla;
    title(str);
    hold on;
    grid on;
    xlim([x y]);
    ylim([x y]);
    text(x+1, y-1, char(string("Current error: "+err)), 'Color', 'black', 'FontSize', 14, 'FontWeight', 'bold');
    text(x+1, y-2, char(string("Current iteration: "+iterations)), 'Color', 'black', 'FontSize', 14, 'FontWeight', 'bold');
    if lastStatus
        text(y-3, x+1, char(string("SUCCESS")), 'Color', 'green', 'FontSize', 14, 'FontWeight', 'bold', 'BackgroundColor', 'black');
    else
        text(y-3, x+1, char(string("FAIL")), 'Color', 'red', 'FontSize', 14, 'FontWeight', 'bold', 'BackgroundColor', 'black');
    end
end

function g = randGoal()
    global negative
    rng('shuffle'); % the seed of the rand = current time
    g = (3.5*rand(1,2)+0.55)' * negative;
    negative = negative * -1;
end
